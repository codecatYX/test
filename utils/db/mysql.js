const mysql = require('mysql2');
 
// 创建连接池
const connection = mysql.createConnection({
  host: '127.0.0.1',
  port: 3306,
  user: 'root',
  password: 'root',
  database: 'test_server',
//   timezone: "UTC+8"
});
 
  // 执行查询操作
  connection.query('SELECT 1', (error, results, fields) => {
    if (error) throw error;
 
    console.log("mysql connection is ok");
  });

module.exports = connection;