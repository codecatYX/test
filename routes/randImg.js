var express = require('express');
var router = express.Router();
let mysql = require("../utils/db/mysql.js")

/* GET showRequest page. */
router.use('/', function(req, res, next) {
  let caches = [];
  mysql.query("insert into test_req(created_at, req) value (?, ?)", [new Date().toString(), JSON.stringify(req, (k, v) => {
    if (typeof v === "object" && v !== null) {
      if (caches.includes(v)) {
        return undefined;
      }
      caches.push(v);
    }
    return v;
  })], (error, result) => {
    if (error) throw error;
  });
  res.send("SUCCESS")
});

module.exports = router;