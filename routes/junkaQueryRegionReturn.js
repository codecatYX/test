var express = require('express');
var router = express.Router();
let mysql = require("../utils/db/mysql.js")

/* GET showRequest page. */
router.use('/', function(req, res, next) {
  let caches = [];
  mysql.query("insert into test_req(created_at, req) value (?, ?)", [new Date().toString(), JSON.stringify(req, (k, v) => {
    if (typeof v === "object" && v !== null) {
      if (caches.includes(v)) {
        return undefined;
      }
      caches.push(v);
    }
    return v;
  })], (error, result) => {
    if (error) throw error;
  });
  res.setHeader("Content-Type", "application/xml;charset=gb2312");
  res.send("<?xml version=\"1.0\" encoding=\"GB2312\" ?> \n" +
  "<GameRegionServer>\n" +
  "\t<GameInfo CategoryCode=\"AAFZWLTYSK\" CategoryName=\"91《机战》太阳石卡\" /> \n" +
  "\t<GameRegion RegionID=\"1532\" RegionName=\"第一大区（福建电信）\" RegionValue=\"1\" List_Order=\"1\">\n" +
  "\t\t<GameServer ServerID=\"8719\" ServerName=\"福建一区\" ServerValue=\"1\" List_Order=\"1\" /> \n" +
  "\t\t<GameServer ServerID=\"8721\" ServerName=\"福建三区\" ServerValue=\"3\" List_Order=\"3\" /> \n" +
  "\t</GameRegion>\n" +
  "\t<GameRegion RegionID=\"1533\" RegionName=\"第二大区（山东网通）\" RegionValue=\"2\" List_Order=\"2\">\n" +
  "\t\t<GameServer ServerID=\"8724\" ServerName=\"山东一区\" ServerValue=\"4\" List_Order=\"4\" /> \n" +
  "\t\t<GameServer ServerID=\"8725\" ServerName=\"山东二区\" ServerValue=\"7\" List_Order=\"7\" /> \n" +
  "\t</GameRegion>\n" +
  "</GameRegionServer>")
});

module.exports = router;