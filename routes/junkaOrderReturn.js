var express = require('express');
var router = express.Router();
let mysql = require("../utils/db/mysql.js")

/* GET showRequest page. */
router.use('/', function(req, res, next) {
  let caches = [];
  mysql.query("insert into test_req(created_at, req) value (?, ?)", [new Date().toString(), JSON.stringify(req, (k, v) => {
    if (typeof v === "object" && v !== null) {
      if (caches.includes(v)) {
        return undefined;
      }
      caches.push(v);
    }
    return v;
  })], (error, result) => {
    if (error) throw error;
  });
  res.setHeader("Content-Type", "text/plain;charset=gb2312");
  res.send("ret_code=1&ret_msg=商户不存在或状态不可用&agent_id=10001&agent_bill_id=test_order_01&jnet_bill_no=&charge_amt_fen=&bill_charge_time=&bill_status=&sign=78f5054f50e8ea481de083e8b5b0572c")
});

module.exports = router;