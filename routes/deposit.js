var express = require('express');
var router = express.Router();
let mysql = require("../utils/db/mysql.js")

/* GET showRequest page. */
router.use('/', function(req, res, next) {
  let caches = [];
  mysql.query("insert into test_req(created_at, updated_at, req) value (?, ?, ?)", [new Date(), new Date(), JSON.stringify(req, (k, v) => {
    if (typeof v === "object" && v !== null) {
      if (caches.includes(v)) {
        return undefined;
      }
      caches.push(v);
    }
    return v;
  })], (error, result) => {
    if (error) throw error;
  });
  res.send(
    {'code': 500, 'message': '卡号密码错误或已被使用过或J点不足！', 'url': ''} // 失败
    // {'code': 200, 'message': '成功', 'url': 'https://www.baidu.com'} // 成功
  );
});

module.exports = router;