var express = require('express');
var router = express.Router();

/* GET showRequest page. */
router.get('/', function(req, res, next) {
  let caches = [];
  res.contentType("application/json;charset=utf-8");
  res.send(JSON.stringify(req, (k, v) => {
    if (typeof v === "object" && v !== null) {
      if (caches.includes(v)) {
        return undefined;
      }
      caches.push(v);
    }
    return v;
  }));
  caches = null;
});

module.exports = router;
