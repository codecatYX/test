var express = require('express');
var router = express.Router();
let mysql = require("../utils/db/mysql.js")

/* GET showRequest page. */
router.get('/:id?', function(req, res, next) {
  const {id} = req.params;
  mysql.query(`select * from test_req where ${id != null ? "id = ?" : "1=1"} order by id desc`, [id], (error, result) => {
    if (error) throw error;
    res.setHeader("Content-Type", "application/json; charset=gb2312");
    res.send(JSON.stringify(result));
  });
});

module.exports = router;
